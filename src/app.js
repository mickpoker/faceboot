require('./assets/styles/index.css');
require('angular');
require('angular-ui-router');

angular.module('app',['ui.router'])
.config(function($stateProvider, $locationProvider, $urlRouterProvider){
	$locationProvider.html5Mode(true);
	$urlRouterProvider.otherwise('/');

	$stateProvider.state("root", {
		url: '/posts',
		component: 'root'
	})
	.state("home", {
		url: '/',
		component: 'home'
	})
	.state("signin", {
		url: '/signin',
		component: 'signin'
	});
});

const context = require.context('.', true, /\.js$/);
context.keys().forEach(context);

console.dir(angular);