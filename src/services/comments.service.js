angular.module('app').service('commentService', function($http){
	var getComments = function(){
		return $http.get('/getComments', function(response){
			return response.data;
		});
	};

	return {
		getComments: getComments
	}
});