angular.module('app').
component('sidebar', {
	template: `<div class="aside"><h1>Users</h1><hr>
				<div ng-repeat="user in model.allComments" class="hoverOnMe" ng-click="model.displayInfo(user.username)">
				<img class="rounder" src="assets/profile.jpg">
				<span>
				{{ user.username }}</span>
				</div></div>`,
	controllerAs: 'model',
	controller: function(commentService){
		var model = this;

		commentService.getComments()
		.then(function(response){
            model.allComments =  response.data;
        });

        model.displayInfo = function(username){
        	alert("Hello, my name is " + username);
        }


	}

});