angular.module('app').
component('navbar', {
	template: '<header class="page-header">' + 
    	'<div class="navbar">' +
        	'<a class="logo" href="#" ng-click="model.goToFacebook()"></a>' + 
        	'<ul class="nav-h">' +
            	'<li>' +
                	'<a class="btn profile profile--hover-bg" href="#" ng-click="model.userInfo()">' +
                    	'<img src="assets/profile.jpg">' +
                    	'<span>{{ model.username }}</span>' +
                	'</a>' +
            	'</li>' +
            	'<li>' +
                	'<a class="btn" href="/">' +
                    	'<i class="fa fa-home" aria-hidden="true"></i>' +
                    	'<span>Home</span>' +
                	'</a>' +
            	'</li>' +
                '<li>' +
                    '<a class="btn" href="/posts">' +
                        '<i class="fa fa-address-book-o" aria-hidden="true"></i>' +
                        '<span>My Posts</span>' +
                    '</a>' +
                '</li>' +
                '<li>' +
                    '<a class="btn" href="/signin">' +
                        '<i class="fa fa-sign-out" aria-hidden="true"></i>' +
                        '<span>{{ model.logout }} </span>' +
                    '</a>' +
                '</li>' +
        	'</ul>' +
    	'</div>' +
	'</header>',
    controllerAs: 'model',
    controller: function($window){
        var model = this;
        model.logout = "Logout";
        model.username = "Username";
        model.goToGoogle = function(){
            $window.location.href = 'http://www.google.com';
        };
        model.goToFacebook = function(){
            $window.location.href = 'http://www.facebook.com';
        };
        model.userInfo = function(){
            alert("Hello, my name is " + model.username + "!");
        };
        
    }
});