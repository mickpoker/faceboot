angular.module('app').component('root', {
	template : "<navbar></navbar>" +
				"<div class='container'>" +
				"<content></content>" + 
				"<sidebar></sidebar></div>" + 
				"<footbar></footbar>"
});