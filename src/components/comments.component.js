angular.module('app').
component('comments', {
	bindings: {
		callback: '&',
		comms: '<',
		postnum: '<'
	},
	template: `<!--USER POST COMMENTS-->
            <ul class="margin-16 top-divider">
                <div class="comments">
                    <!--USER COMMENT-->
                    <li class="flex-col" ng-repeat="com in model.comms">
                        <!--USER COMMENT HEADER-->
                        <div class="flex-between">
                            <a class="profile profile--hover-ul" href="#" ng-click="model.displayInfo(com.username)">
                                <img src="assets/stock_profile.jpg">
                                <span>{{ com.username }}</span>
                            </a>
                            <span class="timestamp text-sm">{{ com.time }}</span>
                        </div>
                        <!--USER COMMENT CONTENT-->
                        <p class="text-md margin-8-top">
                            {{ com.comment }}
                        </p>
                    </li>

                </div>
                <!-- POST INPUT COMPONENT-->
                <div class="p-input">
                    <div contentEditable="true" class="p-input__ta">
						<textarea style="width: 100%; height: 100%; border: none;" ng-model="model.text" placeholder="Add a comment...">
						</textarea>
                    </div>
                    <a class="btn btn-primary f-right margin-8-v" href="#" 
                    	ng-click="model.callback({index: model.postnum, comment: model.text}); model.clearText();">POST</a>
                </div>
            </ul>
            `,
	controllerAs: 'model',
	controller: function(){
		var model = this;
	
		model.displayInfo = function(username){
			alert("Hi my name is " + username + "!");
		};

		model.clearText = function(){
			model.text = "";
		}

		model.$onInit = function(){
			model.postNumber = model.postnum;
		};
	}
});