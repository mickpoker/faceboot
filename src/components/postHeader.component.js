angular.module('app').
component('postHeader', {
	template: `<!--USER POST HEADER-->
        <div class="post" ng-repeat="post in model.allComments">
            <div class="post__header flex-between margin-16">
                <a class="profile profile--lg profile--hover-ul" href="#" ng-click="model.userInfo($index)">
                    <img src="assets/stock_profile.jpg">
                    <span>{{ post.username }}</span>
                </a>
                <span class="timestamp">May 30 at 9:59pm</span>
            </div>
            <!--USER POST CONTENT-->
            <div class="text-md margin-16">
                {{ post.post }}
            </div>
            <!--USER POST FOOTER-->
            <div class="flex-between margin-16">
                <div class="flex" ng-click="model.like($index)">
                    <a class="btn-icon btn--hover-red" href="#"><i class="fa fa-heart fa-lg" ></i></a>
                    <span>{{ post.likes }}</span>
                </div>
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
                    <span>{{ post.comms }}</span>
                </div>
            </div>
            <comments callback="model.increaseComms(index, comment)" comms="post.comments" postnum="$index"></comments>
            </div>`,
    controllerAs: 'model',
    controller: function(commentService){
    	var model = this;
        model.commentNumber = 3;

        var promise = commentService.getComments();
        promise.then(function(response){
            model.allComments =  response.data;
        });

    	model.like = function(index){
            if(model.allComments[index].liked) {
                console.log('its true');
                model.allComments[index].likes --;
                model.allComments[index].liked = false;
            }else{
                console.log('its false');
                model.allComments[index].likes ++;
                model.allComments[index].liked = true;
            }
    	};

    	model.userInfo = function(index){
            alert("Hello, my name is " + model.allComments[index].username + "!");
        };

        // Realistically, this method would send the posted comments back to the back end API and would receive an updated
        // list of comments. For simplicity, I just update the array on the front end.
        model.increaseComms = function(index, comment){
            if(comment){
                model.allComments[index].comms +=1;
                var newComm = {
                 username:'Username' + model.commentNumber++,
                 time: Date().toString(),
                 comment: comment
                };
                model.allComments[index].comments.unshift(newComm);
            }
        };
    }

});