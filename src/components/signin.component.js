// This component was just made to illustrate routing, going from one page to another and handled by angular ui router.

angular.module('app').component('signin', {
	template : `<navbar></navbar>
				<div class='container'>
				<div class="wrap">
				<div class="avatar">
		      	<img src="assets/stock_profile.jpg">
				</div>
				<input type="text" placeholder="username" required>
				<div class="bar">
					<i></i>
				</div>
				<input type="password" placeholder="password" required>
				<a href="/" class="forgot_link">forgot ?</a>
				<button ng-click="$ctrl.goHome()">Sign in</button>
				</div></div>`,

	controller: function($window){
		this.goHome = function(){
			$window.location.href = '/';
		}
	}
});